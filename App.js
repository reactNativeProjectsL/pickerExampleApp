import React, { Component } from 'react';
import {
  StyleSheet,
  Picker,
  View
} from 'react-native';

export default class App extends Component<{}> {
  state = {
    data: [
      {id:1, value:'Java'},
      {id:2, value:'Python'},
      {id:3, value:'JavaScript'},
      {id:4, value:'Ruby'},
      {id:5, value:'Scala'},
    ]
  }

  render() {
    return (
      <View style={styles.container}>
        <Picker
          selectedValue={this.state.language}
          onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
          {
            this.state.data.map((item) => {
              return <Picker.Item label={item.value} value={item.id} key={item.id} />
            })
          }
        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
